ImageHash==4.1.0
Pillow==8.0.0
PyWavelets==1.1.1
numpy==1.19.2
scipy==1.5.2
setuptools==39.1.0
six==1.15.0