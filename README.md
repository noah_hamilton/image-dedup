# Image Deduplicator

This contains a python script that can take images from a directory and identify which are duplicates. A user can specify what directory to do the dedup and where to store the original images and duplicates. The script will not delete any images from your system.

# Supported Image Formats
- png
- jpg
- gif
- svg
- bmp
- tiff

The script will ignore all other file types. More can be added later.

# Hashing
The Image Dedup script utilizes the [ImageHash](https://github.com/JohannesBuchner/imagehash) libary to create the hashes used to determine whether or not a image is a duplicate. Please refer to their documentation for more information

# Using the Image Deduper
1. Please install python3 and pip: https://realpython.com/installing-python/#how-to-install-python-on-windows
2. Pulldown the script files in the repository.
3. In the project directory run "pip3 install -r requirements.txt". This will install all dependencies
4. Create directories where you want to store your original and duplicate images. The script will prompt you for these directory paths.
4. Run the command "python3 dedup.py" in your terminal and wait for the prompts.

# Testing Purposes
If you are wanting to test the program on your system. I recommend going to **line 92** in the _dedup.py_ and commenting that out (add a "#" right before moveImages()).





