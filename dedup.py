import os
import shutil

import imagehash
from PIL import Image

fileList = []
dupImages = {}


def getdirectory():
    while True:
        directory = str(input("Please input image directory to search (e.g. /Users/Images/imageDirectory): "))
        fileSizeMB = 0
        # List Files in Directory
        for root, d_names, files in os.walk(directory):
            for f in files:
                fileList.append(os.path.join(root, f))
                fileSizeMB = float(os.stat(os.path.join(root, f)).st_size / (1024 * 1024)) + fileSizeMB

        print(str(len(fileList)) + " files ("+ str(round(fileSizeMB, 2)) + " MB) were found. Collecting hashes and finding duplicates..")

        if len(fileList) > 0:
            break

        else:
            print("No files/folders found in given directory. Try inputting different directory.")


def isImage(file):
    f = file.lower()
    return f.endswith(".png") or f.endswith(".jpg") or \
           f.endswith(".jpeg") or f.endswith(".bmp") or \
           f.endswith(".gif") or '.jpg' in f or f.endswith(".svg") \
           or f.endswith(".tiff")


def findlikeimages():
    hashes = {}

    for img in sorted(fileList):
        if isImage(img):
            try:
                hash = imagehash.phash(Image.open(img))
            except Exception as e:
                print('Problem:', e, 'with', img)
                continue
            if hash in hashes:
                dupImages[img] = True
            else:
                dupImages[img] = False
                hashes[hash] = hashes.get(hash, [])
    hashes.clear()
    print("Found " + str(sum(value == True for value in dupImages.values())) + " duplicate images")


def moveImages():
    orgdir = False
    dupdir = False
    originalDirectory = None
    duplicatesDirectory = None

    while not orgdir:
        originalDirectory = input(str("Please input an existing directory where I'll store the original images (e.g. "
                                      "/Users/Images/originalDirectory): "))
        orgdir = os.path.isdir(originalDirectory)
        if not orgdir or originalDirectory[-1] == '/':
            orgdir = False
            print("Directory does not exist or was not inputted in the correct format. Try again.")

    while not dupdir:
        duplicatesDirectory = input(str("Please input an existing directory where I'll store the duplicate images ("
                                        "e.g. /Users/Images/duplicateDirectory): "))
        dupdir = os.path.isdir(duplicatesDirectory)
        if not dupdir or duplicatesDirectory[-1] == '/':
            dupdir = False
            print("Directory does not exist or was not inputted in the correct format. Try again.")


    for image in dupImages:
        if not dupImages[image]:
            filename = image.split('/')[-1]
            shutil.move(str(image), originalDirectory + "/" + filename)
        else:
            filename = image.split('/')[-1]
            shutil.move(str(image), duplicatesDirectory + "/" + filename)


if __name__ == '__main__':
    getdirectory()
    findlikeimages()
    moveImages()
